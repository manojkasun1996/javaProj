/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pointofsales;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author User
 */
public class dbconnection {
        
    static String driver="com.mysql.jdbc.Driver";
    static String URL="jdbc:mysql://localhost:3306/point_of_sales";
    static String UserName="root";
    static String Password="";

    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/point_of_sales","root","");
        return con;
    }
    public static void closeconnection(Connection con, ResultSet rs, Statement stmt){
        try{
           if(stmt != null){
            stmt.close();
        }
        if(rs != null){
            rs.close();
        }
        if(con != null){
            con.close();
        }
        }catch(Exception e){
            System.out.println("Failed to close db resources");
        }
    }
}
