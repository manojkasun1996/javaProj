/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pointofsales;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


/**
 *
 * @author User
 */
class LineItem {
        
    private int itemID;
    private int qty;
    private double total;
    private long orderID;
    

    public int getItemID() {
        return itemID;
    }

    public void setItemID(int itemID) {
        this.itemID = itemID;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
    public double getPrice(int itemID) throws ClassNotFoundException{
        Connection con = null;
        Statement stmt = null;
        ResultSet rs=null;
        double price = 0 ;
                try {
            con = dbconnection.getConnection();
            stmt=con.createStatement();
            rs = stmt.executeQuery("select ItemPrice from item where ItemID="+itemID+"");
            while(rs.next()){
            price=(rs.getDouble("ItemPrice"));}
        } catch (SQLException ex) {

        }finally{
            dbconnection.closeconnection(con, rs, stmt);
        }
        return price;
    }
    
     public String getName(int itemID) throws ClassNotFoundException{
        Connection con = null;
        Statement stmt = null;
        ResultSet rs=null;
        String name = null ;
                try {
            con = dbconnection.getConnection();
            stmt=con.createStatement();
            rs = stmt.executeQuery("select ItemName from item where ItemID="+itemID+"");
            while(rs.next()){
            name=(rs.getString("ItemName"));}
        } catch (SQLException ex) {

        }finally{
            dbconnection.closeconnection(con, rs, stmt);
        }
        return name;
    }
    
    public void setTotal(double price,int qty){
         this.total=qty*price;
        
    }
    public double getTotal(){
        return total;
    }
    public void insertLineItem() throws ClassNotFoundException, SQLException{

        try (Connection con = dbconnection.getConnection()) {
            PreparedStatement preparedstmt=con.prepareStatement("insert into lineitem(ItemID,LineItemQuantity,LineItemTotal,OrderNO) values(?,?,?,?)");
            preparedstmt.setInt(1,this.itemID);
            preparedstmt.setInt(2,this.qty);
            preparedstmt.setDouble(3,this.total);
            preparedstmt.setLong(4,this.orderID);
            preparedstmt.executeUpdate();
        } catch (SQLException ex) {
            throw ex;
        }


    }

    public long getOrderID() {
        return orderID;
    }

    public void setOrderID(long orderID) {
        this.orderID = orderID;
    }
    
    
}
