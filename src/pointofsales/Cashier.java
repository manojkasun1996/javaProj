/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pointofsales;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author User
 */
public class Cashier {

    private String cashierID;
    private String cashierPassword;
    private String cashierName;

    public String getCashierID() {
        return cashierID;
    }

    public void setCashierID(String cashierID) {
        this.cashierID = cashierID;
    }

    public String getCashierPassword() {
        return cashierPassword;
    }

    public void setCashierPassword(String cashierPassword) {
        this.cashierPassword = cashierPassword;
    }

    public String getCashierName() {
        return cashierName;
    }

    public void setCashierName(String cashierName) {
        this.cashierName = cashierName;
    }
    
    public String login(String cashierID,String cashierPassword) throws ClassNotFoundException{
        Connection con = null;
        Statement stmt = null;
        ResultSet rs=null;
        String id = null ;
        
            try {
            con = dbconnection.getConnection();
            stmt=con.createStatement();
            rs = stmt.executeQuery("select CashierID,CashierPassword from cashier where CashierID='"+cashierID+"'and CashierPassword='"+cashierPassword+"'");
            if(!rs.next()){
              return "-1";  
            }
            else{
                id=(rs.getString("CashierID"));
            }
        } catch (SQLException ex) {

        }finally{
            dbconnection.closeconnection(con, rs, stmt);
        }
        return id;
    }
}
