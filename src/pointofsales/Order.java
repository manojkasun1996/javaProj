/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pointofsales;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author User
 */
public class Order {
    private long orderID;
    private String cashierID;
    private Timestamp timeStamp;
    private List<LineItem> lineItems;
    
 
    
    
    public long getOrderID() {
        return orderID;
    }

    public void setOrderID(long orderID) {
        this.orderID = orderID;
    }

    public String getCashierID() {
        return cashierID;
    }

    public void setCashierID(String CashierID) {
        this.cashierID = CashierID;
    }

    public Timestamp getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Timestamp timeStamp) {
        this.timeStamp = timeStamp;
    }

    public List<LineItem> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<LineItem> lineItems) {
        this.lineItems = lineItems;
    }

//    line Item ekathu karala order eke total eka gannawa
    public double getOrderTotal (List<LineItem> lineItems){
        double total =0;
        for(LineItem l : lineItems){
            total = total + l.getTotal();
        }
        return total;
    }
    
//    order eka db ekata write krnawa
    public void insertOrder (String cashierID,double total) throws ClassNotFoundException{


        try (Connection con = dbconnection.getConnection()) {
            PreparedStatement preparedstmt=con.prepareStatement("insert into order_item(CashierID,OrderTotal) values(?,?)");
            preparedstmt.setString(1,cashierID);
            preparedstmt.setDouble(2,total);
            preparedstmt.executeUpdate();
            Statement stmt=con.createStatement();
            ResultSet rs = stmt.executeQuery("select max(OrderNO) as max_id from order_item ");
            rs.next();
            for(LineItem lineItem:this.lineItems){
                lineItem.setOrderID(rs.getInt("max_id"));
                lineItem.insertLineItem();
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }


    }
}
